#!/usr/bin/python
"""* ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * OneEyedDancer/TakPlintus wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.
 * Details: https://www.youtube.com/watch?v=dQw4w9WgXcQ
 * ----------------------------------------------------------------------------"""

from random import randint, choice
import tkinter
import webbrowser
import subprocess
import getpass
import ctypes
import time
import sys
import os

try:
    import requests
    from plyer import notification
    from bs4 import BeautifulSoup
    import colorama

    """Цвета для colorama
    Чёрный      30  40
    Красный     31  41
    Зелёный     32  42
    Жёлтый      33  43
    Синий       34  44
    Фиолетовый  35  45
    Бирюзовый   36  46
    Белый       37  47
        Эффекты для colorama
    0   Сброс к начальным значениям
    1   Жирный
    2   Блёклый
    3   Курсив
    4   Подчёркнутый
    5   Редкое мигание
    6   Частое мигание
    7   Смена цвета фона с цветом текста"""
except ModuleNotFoundError:
    print('Обнаружены не установленные пакеты!')
    input('Продолжая установку вы соглашаетесь принять ислам')
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'requests', 'colorama', 'bs4', 'plyer'])

    import requests
    from plyer import notification
    from bs4 import BeautifulSoup
    import colorama

    if os.name == 'nt':
        os.system('CLS')
    else:
        os.system('сlear')

game_over = '''\033[31m
  ▄████  ▄▄▄       ███▄ ▄███▓▓█████     ▒█████   ██▒   █▓▓█████  ██▀███  
 ██▒ ▀█▒▒████▄    ▓██▒▀█▀ ██▒▓█   ▀    ▒██▒  ██▒▓██░   █▒▓█   ▀ ▓██ ▒ ██▒
▒██░▄▄▄░▒██  ▀█▄  ▓██    ▓██░▒███      ▒██░  ██▒ ▓██  █▒░▒███   ▓██ ░▄█ ▒
░▓█  ██▓░██▄▄▄▄██ ▒██    ▒██ ▒▓█  ▄    ▒██   ██░  ▒██ █░░▒▓█  ▄ ▒██▀▀█▄  
░▒▓███▀▒ ▓█   ▓██▒▒██▒   ░██▒░▒████▒   ░ ████▓▒░   ▒▀█░  ░▒████▒░██▓ ▒██▒
 ░▒   ▒  ▒▒   ▓▒█░░ ▒░   ░  ░░░ ▒░ ░   ░ ▒░▒░▒░    ░ ▐░  ░░ ▒░ ░░ ▒▓ ░▒▓░
  ░   ░   ▒   ▒▒ ░░  ░      ░ ░ ░  ░     ░ ▒ ▒░    ░ ░░   ░ ░  ░  ░▒ ░ ▒░
░ ░   ░   ░   ▒   ░      ░      ░      ░ ░ ░ ▒       ░░     ░     ░░   ░ 
      ░       ░  ░       ░      ░  ░       ░ ░        ░     ░  ░   ░     
                                                     ░                   

\033[0m'''


def game_exit():
    input()
    sys.exit()


def player_say(text):
    print(f'\033[33m[{player_name}]\033[0m: {text}', end=' ')
    input()


def conscience_say(text):
    print(f'\033[36m[{conscience_name}]\033[0m: {text}', end=' ')
    input()


def other_say(name, text):
    print(f'\033[35m[{name}]\033[0m: {text}', end=' ')
    input()


def add(item):
    inventory.append(item)
    print(f'~{item} добавлен в инвертарь')


def loading(t):
    states = ['\r    /', '\r    -', '\r    \\', '\r    |']
    for _ in range(t):
        for j in states:
            print(f'{j}  Loading', end='')
            time.sleep(1)
    print('\r', end='')


def clear():
    if os.name == 'nt':
        os.system('CLS')
    else:
        os.system('сlear')
    print('     ' * 10, end='')
    print(f'\033[33m[PETRIKOR TECHNOLOGIES][V1|{os.path.getsize("PETRIKOR.py") // 1024}KB]\033[0m')


def BSOD():
    url = 'https://ie.wampi.ru/2022/01/03/dead.png'
    img_data = requests.get(url).content
    with open('dead.png', 'wb') as handler:
        handler.write(img_data)

    window = tkinter.Tk()
    window['bg'] = '#0079d6'
    filename = tkinter.PhotoImage(file='dead.png')
    background_label = tkinter.Label(window, image=filename, background='#0079d6')
    background_label.place(x=0, y=0, relwidth=1, relheight=1)

    if os.name == 'nt':
        window.attributes('-fullscreen', True)
    else:
        window.attributes('-zoomed', True)

    window.mainloop()


def achievement(text):
    file = open('Прогресс.txt', 'r')
    file_ = list(file)
    for i in range(len(file_)):
        if text in file_[i]:
            if '[-]' in file_[i]:
                file_[i] = file_[i].replace('[-]', '[+]')
                break
            else:
                return 1
    file.close()
    file = open('Прогресс.txt', 'w')
    [file.write(i) for i in file_]
    file.close()

    notification.notify(
        title='Получено новое достижение!',
        message=text,
        app_name='PETRIKOR'
    )


def create_file():
    achievements = {'Имя': player_name,
                    1: 'Контракт с Дьяволом',
                    2: 'Удар дракона',
                    3: 'Мыслитель',
                    4: 'ВРЕМЯ ПЕРЕУСТАНАВЛИВАТЬ ШИНДОВС!',
                    5: 'Дымовой',
                    6: 'Павший',
                    7: 'Путь коммунизма',
                    8: 'Путь капитализма',
                    9: 'Вызов чёрта',
                    10: 'Торч',
                    11: 'Потерянный',
                    12: 'Зубочистка',
                    13: 'ГОП-СТОП',
                    14: 'О, вы из Махаве?',
                    15: 'Мухожук',
                    16: 'Взлом жопы',
                    17: 'Вакцинированный от кринжа',
                    18: 'Убитый пельменями',
                    19: 'Капитан очевидность',
                    20: 'Каскадер',
                    21: 'Алкоголик',
                    22: 'Легенды не умирают',
                    23: 'Ничто не истинно, всё дозволено'}

    with open('Прогресс.txt', 'w') as file:
        file.write(f"[  {achievements.get('Имя')}   ]\n")
        for i in range(1, len(achievements)):
            file.write(f'{i}. {achievements.get(i)} [-]\n')


# начало
colorama.init()
print('\033[36m(ↄ)TakPlintus  |  Beerware License\033[0m')

print('''\033[33m║░░█████░█████░█████░████░░█░░░█░█░░█░█████░████
\033[33m║░░█░░░█░█░░░░░░░█░░░█░░░█░█░░░█░█░░█░█░░░█░█░░░█
\033[33m║░░█░░░█░█░░░░░░░█░░░█░░░█░█░░░█░█░█░░█░░░█░█░░░█
\033[33m║░░█░░░█░█████░░░█░░░\033[0m████░░█░░██░██░░░█░░░█░\033[33m████
\033[33m║░░█░░░█░█░░░░░\033[0m░░█░░░█░░░░░█░█░█░█░█░░█░░░█░█
\033[33m║░░█░░░█░\033[0m█░░░░░░░█░░░█░░░░░██░░█░█░░█░█░░░█░█
\033[33m║░░█░░░\033[0m█░█████░░░█░░░█░░░░░█░░░█░█░░█░█████░█\033[0m''')
print('\033[33m\Нажмите на Enter для продолжения\033[0m ')
input()

print('Добро пожаловать в \033[33mПЕТРИКОР: 3-YEARS ANNIVERSARY\033[0m')

if not os.path.isfile('Прогресс.txt'):
    player_name = input('Введите ваше никчемное имя > ')
    if player_name == '':
        player_name = 'Шизофреник'

    print('''\a(r)Если желаете выйти уже сейчас
(a)Если хотите продолжить страдать фигней''')

    dialog_1 = input('~> ')
    if dialog_1 == 'r':
        print('Иди своей дорогой сталкер')
        webbrowser.open_new('https://www.youtube.com/watch?v=sT8m1Rk0oCU')  # GAME OVER MGS
        print(game_over)
        game_exit()
    elif dialog_1 == 'a':
        input('Благодарим за участие в эксперименте! ')
        clear()
    else:
        print('Иди своей дорогой сталкер')
        webbrowser.open_new('https://youtu.be/F5R3fEqq8tM')  # пустошь
        print(game_over)
        game_exit()
    create_file()
else:
    with open('Прогресс.txt', 'r') as file:
        player_name = list(file)[0][3:-5]

achievement('Контракт с Дьяволом')
conscience_name = 'Сознание'
inventory = []

input('*Стук в дверь...* ')
player_say('Входите!')
conscience_say('До тебя доходит осознание')
player_say('Почему я кого-то впустил? И где я сам?')
player_say('Хмм...')
player_say('Я нашел ключ и записку')
add('Ключ')

conscience_say('Записка выглядит интересно, может стоит взглянуть на неё?')
print('(r)Да\n(a)Нет')
dialog_2 = input('~> ')
if dialog_2 == 'r':
    print('--------------------------------------------')
    print('"Я обязан передавать то, что говорят, \nно верить каждому крокодилу я не обязан, \nпоэтому я должен отнести '
          'эту хрень" ')
    print('--------------------------------------------', end='')
    input()
    player_say('Также здесь есть цифры')
    print('\033[31m191\033[0m' + '\033[33m7\033[0m ')
    player_say('Думаю это стоит запомнить')
else:
    player_say('Я чё по вашему крокодил, чтобы это читать')

conscience_say('Похоже в этой комнате больше ничего не осталось')
player_say('Пора выходить из этого нарко-питона!')
input('*Ba Dum Tss* ')
conscience_say('!?')
print('\033[31mНанесен серьезный вред вашему списку каламбуров\033[0m', end='')
input()

conscience_say('Вы вообразили себя мастером по вышибанию дверей')
player_say('Hadouken!')
input('*Дверь кродется*')
conscience_say('Действительно стоит отсюда уходить? Может что-то забыли?')
print('Вы хотите покинуть эту локацию?\n(r)Естественно\n(a)Ни в коем случае')
dialog_3 = input('~> ')
if dialog_3 == 'a':
    player_say('Сегодня был тяжелый день')
    player_say('О пивасик!!!')
    achievement('Алкоголик')
    conscience_say('Ваш разум тумнеет')
    webbrowser.open_new('https://www.youtube.com/watch?v=CqNROz3agJs')  # Алкаш упал
    conscience_say('Пивко для рывка... Байка, как и думал')
    print(game_over)
    game_exit()
elif dialog_3 == 'r':
    player_say('Не нужно задерживаться')
else:
    conscience_say('Вы не решительны')
    player_say('О пивасик!!!')
    achievement('Алкоголик')
    conscience_say('Ваш разум тумнеет')
    webbrowser.open_new('https://www.youtube.com/watch?v=CqNROz3agJs')  # Алкаш упал
    conscience_say('Пивко для рывка... Байка, как и думал')
    print(game_over)
    game_exit()

conscience_say('Вы замечаете как дверь встала и вернулась на свое место')
conscience_say('Вы это просто так оставите?')
print('''\033[35m(r)\033[0mВернуться к своим делам
\033[35m(a)\033[0mДать леща
\033[35m(i)\033[0mСломать колени
\033[35m(n)\033[0mООО ПИВКО НАШЛОСЬ! ''')
dialog_4 = input('~> ')
if dialog_4 == 'r':
    player_say('Какое мне дело до этой двери, не шпион же')
elif dialog_4 == 'a':
    achievement('Удар дракона')
    conscience_say('Вы дали самого смачного леща в своей жизни')
    conscience_say('Вы обидели дверь')
elif dialog_4 == 'i':
    achievement('Каскадер')
    conscience_say('Вы сломали себе колени')
    conscience_say('Вы малолетний дебил')
    webbrowser.open_new('https://www.youtube.com/watch?v=b1Y7597gGj8')
    print(game_over)
    game_exit()
elif dialog_4 == 'n':
    player_say('Это пиво выглядит неплохо')
    achievement('Алкоголик')
    player_say('*Неразборчиво*')
    webbrowser.open_new('https://www.youtube.com/watch?v=CqNROz3agJs')
    conscience_say('Пивко не для рывка, байка, как и думал')
    print(game_over)
    game_exit()
else:
    conscience_say('Вы проигнорировали дверь')

conscience_say('Выйдя из этого замечательного здания, вы начали осматривать окрестности')
player_say('Я без понятия что это за город, хотя после пробуждения, я толком ничего и не помню')

conscience_say('Есть табличка с названием улицы')
player_say('Хмм... "\033[31mПереу\033[0m\033[35mл\033[0m\033[31mок нечист\033[0m\033[35mо\033['
           '0m\033[31mй си\033[0m\033[35mл\033[0m\033[31mы 13\033[0m"')
conscience_say('Стоит проверить в своём телефоне')
input('*Телефона в кармане нет*')
player_say('Зараза')

conscience_say('Смирившись с потерей, продолжили рассматривать улицу')
conscience_say('Замечен небольшой магазин')
player_say('Проверим')
loading(2)
conscience_say('Вы заходите в магазин...')
other_say('Незнакомец', 'Привет \033[32mдружище\033[0m, давно я тебя не видел в уличных '
                        'гонках')
conscience_say('Капаясь в глубине своих оставшихся воспоминаниях, пытаетесь вспомнить его имя')
print('(r)Продолжить вспоминать[30%]\n'
      '(a)Бросить это гиблое дело')
dialog_5 = input('~> ')
if dialog_5 == 'r':
    if randint(0, 100) >= 70:
        print('\033[42mУспешно\033[0m')
        achievement('Мыслитель')
        player_say(f'Точно, ты же тот самый Урювкос')
        conscience_say('Вы вспомнили только его имя')
    else:
        print('\033[41mПРОВАЛ\033[0m')
        other_say('Урювкос', 'Ах ты псинус, я же великий Урювкос')
        conscience_say('Вам это ничего не дало')
else:
    other_say('Урювкос', 'Ах ты псинус, я же великий Урювкос')
    conscience_say('Вам это ничего не дало')

clear()
print('\a\033[41m!Сигнал прерывается!\033[0m')
loading(1)
conscience_name = '*^|#$%@'
conscience_say('Эй! Какого хрена всё повисло!')
conscience_say('Мы настраивали это не первый месяц')
conscience_say('И почему же тогда всё заглохло?')
loading(1)
conscience_say('\033[33m0ЮШМИНАЛЬД!!!\033[0m')
conscience_say('Р0Т ТВ0ЕГО КАЗИНО МЛЯТЬ, ТЫ ГДЕ К0Д ЗАРЯЖАЛ')
conscience_say('МЫ ИЗ-ЗА ТЕБЯ ТЕРЯЕМ ВЫЙГРЫШ 0Т ЖИРА ФИКУСА!')
print('\033[31m|--------------------------------------------\033[0m')
print('\033[31m|\033[0m' + time.ctime())
print('\033[31m|--------------------------------------------\033[0m')
print('\033[41mТЕХНИЧЕСКИЕ НЕПОЛАДКИ\033[0m')
loading(3)
clear()
other_say('Оюшминальд', 'Дело сделано')
conscience_name = 'Лор#@$&*!'
conscience_say('Отлично, продолжаем действовать!')
loading(1)
conscience_say(f'Товарищ {getpass.getuser()}')
conscience_say('Придется сделать важный выбор')
conscience_say('Я знаю что ты не готов, но это нужно...')
conscience_say('От этого зависит жизнь твоего электроприбора')
conscience_say('Выбирай')
print('\033[41m(r)Земля тебе 0xc000000e\033[0m\n'
      '(a)Играть в дерьмовый квест')
dialog_6 = input('~> ')
if dialog_6 == 'r':
    achievement('ВРЕМЯ ПЕРЕУСТАНАВЛИВАТЬ ШИНДОВС!')
    print('\033[41m:)\033[0m')
    time.sleep(2)
    BSOD()
elif dialog_6 == 'a':
    conscience_say('Вы уверены в своём решении? Выбрать первый вариант (r)')
    if input('~> ') == 'r':
        achievement('ВРЕМЯ ПЕРЕУСТАНАВЛИВАТЬ ШИНДОВС!')
        print('\033[41m:)\033[0m')
        time.sleep(2)
        BSOD()
        input()
        game_exit()
    else:
        conscience_say('Ладно, как пожелаете')
else:
    conscience_say('Молчим? Приму это как ответ')
    print('\033[41m:)\033[0m')
    time.sleep(2)
    BSOD()
    input()
    game_exit()

clear()
print('\033[46m!Переподключение к серверу!\033[0m')
loading(1)
conscience_name = 'Сознание'
conscience_say('Урювкоса поблизости не видно, видимо ушел')
input('~Входящая мысль')
conscience_say('Вы, по непонятной вами причине, вспомнили нахождение \033[32mgrove street home\033[0m')
conscience_say('Перекусить перед походом?')
print('(r)Да\n(a)Нет, спасибо, я не голодный')
dialog_7 = input('~> ')
if dialog_7 == 'r':
    conscience_say('''Вы покупаете:
    a 2 number 9s
    a number 9 large
    a number 6 with extra dip,
    a number 7
    two numbers 45s
    one with cheese
    and a large soda''')
    achievement('Дымовой')
    conscience_say('Вы думали, что смогли бы побить рекорд крысы, но сегодня')
    webbrowser.open_new('https://www.youtube.com/watch?v=ulEisLcFEl4')
    print(game_over)
    game_exit()
else:
    player_say('Не сегодня')

player_say('Дом, жди меня!')
loading(2)
webbrowser.open('https://www.youtube.com/watch?v=HOhIHEewsDM')  # Армяне играют в нарды
loading(1)
clear()
conscience_say('\a\033[41mALERT! Приближается враг!\033[0m')
print('/-------------------------------------(*)')
print('| Бомж:       ___________             |')
print('| Василий    /           |            |')
print('| 20 лет    | _|__|__|___ |           |')
print('|            |           |            |')
print('|            | =      \033[36m0\033[0m  |            |')
print('|            |    ∆     # |           |')
print('|            |____________|           |')
print('| °•                                  |')
print('[========∆===================©========/')
conscience_say('Любитель дешевых трубок выглядит чрезвычайно угрожающе')
conscience_say('Куда ваше тело желает увернуться(упасть)?')
print('''(r)Влево, под машину
(a)Вправо, сброситься с моста
(i)\033[31mЛегкий путь\033[0m''')
dialog_8 = input('~> ')
if dialog_8 == 'r':
    conscience_say('\033[32mВы увернулись как мешок с картошкой\033[0m')
    conscience_say('Лучше ударить первым')
    conscience_say('Какой вид атаки предпочитаете?')
    print('''\033[36m(r)\033[0mЯ вам запрещаю срать
\033[36m(a)\033[0mЯ слышал, что недавно выпустили новую трубку с 6 плитками
\033[36m(i)\033[0mВерх верх вниз вниз влево вправо B A
\033[36m(n)\033[0m\033[31mВыйти из системы\033[0m''')
    dialog_8_1 = input('~> ')
    if dialog_8_1 == 'r':
        conscience_say('Что вы ждали от этого выбора?')
        conscience_say('Естественно вам дали в морду')
        webbrowser.open_new('https://youtu.be/-1qju6V1jLM')  # ah shet
        print(game_over)
        game_exit()
    elif dialog_8_1 == 'a':
        other_say('Бомж', 'Я РАБОТЯГА! Я РАБОТАЮ ТРИ ДНЯ! УАГХХ')
        other_say('Бомж', 'НА В МОРДУ!')
        webbrowser.open_new('https://youtu.be/3aMXpBm-hgQ')  # салями
        print('''\033[31m
 ▄         ▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄▄  ▄▄▄▄▄▄▄▄▄▄  
▐░▌       ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ 
▐░▌       ▐░▌▐░█▀▀▀▀▀▀▀█░▌▐░█▀▀▀▀▀▀▀▀▀  ▀▀▀▀█░█▀▀▀▀ ▐░█▀▀▀▀▀▀▀▀▀ ▐░█▀▀▀▀▀▀▀█░▌
▐░▌       ▐░▌▐░▌       ▐░▌▐░▌               ▐░▌     ▐░▌          ▐░▌       ▐░▌
▐░▌   ▄   ▐░▌▐░█▄▄▄▄▄▄▄█░▌▐░█▄▄▄▄▄▄▄▄▄      ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░▌       ▐░▌
▐░▌  ▐░▌  ▐░▌▐░░░░░░░░░░░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░▌       ▐░▌
▐░▌ ▐░▌░▌ ▐░▌▐░█▀▀▀▀▀▀▀█░▌ ▀▀▀▀▀▀▀▀▀█░▌     ▐░▌     ▐░█▀▀▀▀▀▀▀▀▀ ▐░▌       ▐░▌
▐░▌▐░▌ ▐░▌▐░▌▐░▌       ▐░▌          ▐░▌     ▐░▌     ▐░▌          ▐░▌       ▐░▌
▐░▌░▌   ▐░▐░▌▐░▌       ▐░▌ ▄▄▄▄▄▄▄▄▄█░▌     ▐░▌     ▐░█▄▄▄▄▄▄▄▄▄ ▐░█▄▄▄▄▄▄▄█░▌
▐░░▌     ▐░░▌▐░▌       ▐░▌▐░░░░░░░░░░░▌     ▐░▌     ▐░░░░░░░░░░░▌▐░░░░░░░░░░▌ 
 ▀▀       ▀▀  ▀         ▀  ▀▀▀▀▀▀▀▀▀▀▀       ▀       ▀▀▀▀▀▀▀▀▀▀▀  ▀▀▀▀▀▀▀▀▀▀  
                                                                              
\033[0m''')
        game_exit()
    elif dialog_8_1 == 'i':
        print('\033[42mЧИТ-КОД АКТИВИРОВАН\033[0m')
        other_say('Бомж', 'ТЫ ШО, ЕБОБО?')
        other_say('Бомж', 'Ну нахрен')
        add('Деньги')
        conscience_say('ВЫ одержали великую победу!')
    elif dialog_8_1 == 'n':
        clear()
        print('\033[41m Выход из системы... \033[0m')
        conscience_name = 'Лориэрик'
        conscience_say('Прощай...')
        os.system('shutdown -s')
        achievement('Павший')
        webbrowser.open_new('https://youtu.be/FeCXooh8AXE')  # Да не бомбит у меня
        print(game_over)
        game_exit()
    else:
        conscience_say('Уснули?')
        conscience_say('Естественно, вам дали в морду')
        webbrowser.open_new('https://youtu.be/-1qju6V1jLM')  # ah shet
        print(game_over)
        game_exit()
elif dialog_8 == 'a':
    achievement('Ничто не истинно, всё дозволено')
    player_say('НИЧТО НЕ ИСТИННО, ВСЁ ДОЗВОЛЕНО!')
    player_say('АААА')
    conscience_say('Вы прыгнули с моста')
    print('/-------------------------------------/')
    print('|                                     |')
    print('|               ||                    |')
    print('|               []                    |')
    print('|              /0\                    |')
    print('|                                     |')
    print('|            ########                 |')
    print('|          \__________/               |')
    print('| °•        о        о                |')
    print('[========∆===================©=======+/')
    conscience_say('Вам в колено попала стрела, которая лежала в стоге сена')
    clear()

    print('\033[41mВ ОТКЛЮЧКЕ\033[0m')
    loading(2)
    other_say('Ботинок', '\033[31mПросыпайся, Ты обосрался! Но ты нужен нам, \033[0m\033[33mвставай\033[0m')
    other_say('Ботинок', 'О господи, спаси это велосипед')
    other_say('Ботинок', 'Привет, цирк выступает в антарктиде')
    player_say('Иди лечись брат...')
    other_say('Ботинок', ':(')
    loading(1)
    clear()
    print('\033[46mВЫ ПРОСНУЛИСЬ\033[0m')
elif dialog_8 == 'i':
    clear()
    print('\033[41m Выход из системы... \033[0m')
    conscience_name = 'Лориэрик'
    conscience_say('Прощай...')
    os.system('shutdown -s')
    achievement('Павший')
    webbrowser.open_new('https://youtu.be/FeCXooh8AXE')  # Да не бомбит у меня
    print(game_over)
    game_exit()
else:
    clear()
    print('\033[41m ПЕРЕЗАГРУЗКА... \033[0m')
    conscience_name = 'Лориэрик'
    conscience_say('Почему же ты ничего не ответил?')
    conscience_say('Самый умный?')
    conscience_say('Иди начинай заново')
    webbrowser.open_new('https://youtu.be/-1qju6V1jLM')  # ah shet
    game_exit()

player_say('Кажется дождь начинается')
player_say('А что это за упомрачительная вещь?')
print('/-------------------------------------/')
print('| Обьект:                             |')
print('1 ???             /                   |')
print('|               /   \                 |')
print('|             /_______\               |')
print('|           |     /     |             |')
print('|             \ /     /               |')
print('|               \   /                 |')
print('| °•              \                   |')
print('[========∆===================©=======-7')
add('???')
player_say('Выглядит недурно, заберу с собой')
loading(2)

clear()
conscience_say('Вы подходите к своему дому')
player_say('Тэкс...')
player_say('Ключ не тот-то')
player_say('Другого у меня нет')

player_say('Попробуем позвонить в домофон')


def doorbell():
    clear()
    print('       \033[44mРЕЖИМ СВЯЗИ\033[0m')
    print('''            ╓▒▒▒▒▒▒▒▒▒
            ║        ▒
            ▒ ░░░░░░ ▒
            ▒ ░░░░░░ ▒
            ▒        ▒
            ▒ ▓▓▓▓▓▓ ▒
            ▒ ▓#▓#▓▓ ▒
            ▒ ▓▓▓▓▓▓ ▒
            ▒ ▓#▓#▓▓ ▒
            ▒ ▓▓▓▓▓▓ ▒
            ▒        ║
            ▒▒▒▒▒▒▒▒▒╝

                    ''')
    conscience_say('0-9999')
    number = input('~> ')

    print('*Звонок*')
    loading(1)
    if number == '1917':
        other_say('Комуняка', 'Ааа?')
        other_say('Комуняка', 'Кто меня разбудил?')
        player_say('Я...')
        other_say('Комуняка', 'Ну проходи...')
        print('*Бубнит что-то про малолетних дебилов*')
        achievement('Путь коммунизма')
    elif number == '100':
        other_say('Капитал прожиточного минимума', 'Ну как там с деньгами?')
        player_say('Какими...')
        if 'Деньги' in inventory:
            player_say('Конечно есть')
            other_say('Капитал прожиточного минимума', 'Проходи')
            inventory.remove('Деньги')
            achievement('Путь капитализма')
        else:
            other_say('Капитал прожиточного минимума', 'Нет денег, проваливай!')
            return doorbell()
    elif number == '666':
        other_say('Чертила', 'Какой мудила звонит мне?')
        conscience_say('Лихорадочно сбрасываете звонок...')
        achievement('Вызов чёрта')
        return doorbell()
    elif number == '228':
        other_say('Торч0к', 'ПрИвЕт БрАтОк')
        other_say('Торч0к', 'НыТ Nичег0 слАщЕ, чУм дутЪ с0вет')
        other_say('Торч0к', 'ЗАхОдИ иИ пР0знАЕшь вЕСЬ СуП, я 0Ткр0ю ТеБе вСраТа')
        player_say('Звучит заманчиво')
        loading(2)
        webbrowser.open_new('https://youtu.be/MXcMV-d_2Js')  # one hour later
        player_say('Какого хрена в этом доме не работает лифт')
        input('*Стук*')
        other_say('Торч0к', 'А в0т И тЫ брАтИшка')
        other_say('Торч0к', 'я ДУМал тЫ УжЕ нЕ пРиЛетИтъ')
        other_say('Торч0к', 'рАСсП0лаГаЙся')
        conscience_say('Вы замечаете своих старых друзей')
        other_say('Ботинок', 'Бурят из казино на месте')
        other_say('Урювкос', 'Здарова гонщик!')
        other_say('Оюшминальд', f'Привет {player_name} и {bin(int.from_bytes("Лориэрик".encode(), "big"))[2:]}')
        player_say('Ооо, тут можно сладко дунуть!')
        webbrowser.open_new('https://youtu.be/MJbE3uWN9vE')  # baka mitai
        achievement('Торч')
        player_say('Да не торч я!')
        print(game_over)
        game_exit()
    elif number == '404':
        other_say('Ошибка сервера', 'Ты кто такой?')
        other_say('Ошибка сервера', 'Я тебя не звал')
        other_say('Ошибка сервера', 'Катись отсюда')
        achievement('Потерянный')
        return doorbell()
    elif number == '0451':
        clear()
        print('\033[46mПодключение...\033[0m')
        loading(2)

        if os.name == 'nt':
            url = 'https://i.playground.ru/p/9UcaoBL7rm173zM3iPsFhw.jpeg'
            img_data = requests.get(url).content
            with open('lock.jpg', 'wb') as handler:
                handler.write(img_data)
            path = f'{os.getcwd()}/lock.jpg'

            ctypes.windll.user32.SystemParametersInfoW(20, 0, path, 0)
        else:
            print('\033[41mПровал\033[0m')
            achievement('Зубочистка')
            input()
            return doorbell()
        achievement('Зубочистка')
        return doorbell()
    else:
        print('\033[41m404 error: not found\033[0m')
        conscience_say('Стоит попробовать другой номер')
        return doorbell()


doorbell()

clear()
player_say('89 кввартира на 38 этаже')
conscience_say('Идти пешком или на лифте?')
conscience_say('Да, я понимаю, это тяжелый выбор')
print('(r)Пешком\n(a)На лифте[Шанс на выживание равен 50%]')
dialog_9 = input('~> ')
if dialog_9 == 'r':
    if randint(0, 100) >= 50:
        conscience_say('Вы спокойно прошли')
    else:
        other_say('Гопник', 'Гони деньги!')
        if 'Деньги' in inventory:
            print('\033[42mУспешно\033[0m')
            player_say('Держи')
            other_say('Гопник', 'Проходи')
        else:
            print('\033[41mПРОВАЛ\033[0m')
            other_say('Гопник', 'Нет?!')
            other_say('Гопник', 'БАН!')
            achievement('ГОП-СТОП')
            webbrowser.open_new('https://youtu.be/AVssMNShgso')  # гопарь
            print(game_over)
            game_exit()
elif dialog_9 == 'a':
    if randint(0, 100) >= 50:
        print('\033[42mУспешно\033[0m')
        player_say('Вполне нормальный лифт')
    else:
        print('\033[41mПРОВАЛ\033[0m')
        achievement('О, вы из Махаве?')
        print('\033[41mВас поглатила пустошь и запах лифта\033[0m', end='')
        input()
        webbrowser.open_new('https://youtu.be/F5R3fEqq8tM')  # пустошь
        print(game_over)
        game_exit()
else:
    player_say('Я устал, я мухожук')
    achievement('Мухожук')
    webbrowser.open_new('https://www.youtube.com/watch?v=sT8m1Rk0oCU')  # GAME OVER MGS
    print(game_over)
    game_exit()

loading(2)
clear()
player_say('Кхм')
print('*Стук*')
loading(1)
player_say('Похоже дома никого нет')
player_say('Тут есть отверстие под ключ похожий на мой')
player_say('Попробуем-ка')
loading(1)
print('*Дверь открылась*', end='')
input()

print('                                                        ________       ')
print('                  ______________________               /    |   \      ')
print('                  |         |           |\ _          |     |    |     ')
print('                  |       \033[34m/\033[0m |     \033[34m/\033[0m      |   \_       |_____|    |     ')
print('                  |         |            |     \_     |          |     ')
print('                  |   \033[34m/\033[0m     |       \033[34m/\033[0m    |       |     \________/      ')
print('                  |         |   \033[34m/\033[0m        |       |                     ')
print('                  |      \033[34m/\033[0m  |            |       |                     ')
print('                  |         |            |       |                     ')
print('                  |    \033[34m/\033[0m    |      \033[34m/\033[0m     |       |                     ')
print('    ________      |         |   \033[34m/\033[0m        |       |                     ')
print('   |        |     |       \033[34m/\033[0m |            |       |                     ')
print(' __|________|__   |   \033[34m/\033[0m     |   \033[34m/\033[0m        |       |                     ')
print('        |         |         |        \033[34m/\033[0m   |       |                     ')
print('        |          ______________________ \_     |                     ')
print('        |                                    \_  |                     ')
print('       / \                                     \_|                     ')

player_say('Дождь ещё идёт')
player_say('Кажется я вспомнил...')
player_say('Это моя комната, маленькая и уютная')
player_say('Я \033[31mлюбил\033[0m её...')
player_say('В ней помещались все нужные и не нужные вещи - биван, краденный телек, шахматы, консось новего покаления, '
           'часы "Стукач", которые меня палили, а также холодильник "Отмороженный"')
player_say('А этого \033[33mсундучка\033[0m у меня не было')
player_say('Ключ все ещё у меня')
player_say('Действуем...')
loading(1)
print('\n\033[42mУспешно\033[0m')
conscience_say('Вы смогли открыть сундучок, но сломали ключ')
achievement('Взлом жопы')
print('\033[46mНавык "Взлом" повышается до 1\033[0m', end='')
input()
clear()

print('_-_______---____________-_-_______----______          ')
print('|                                           |           ')
print('|                                           |           ')
print('|                                           |           ')
print('|  \033[47m\033[30mХаХАХАХАХА\033[0m\033[0m                               |')
print('|                                           |           ')
print('|             \033[47m\033[30mТЫ ДУМАЛ ЧТО Тут\033[0m\033[0m              |')
print('|                                           |         ')
print('|   \033[47m\033[30mЧТО-тО ЕСТЬ\033[0m\033[0m                             |')
print('|                                           |           ')
print('|                                           |           ')
print('|                                           |           ')
print('|                        \033[33mТвой сосед Богдан\033[0m  |')
print('|___________________________________________|          ')
input()
player_say(
    'И я ради этого \033[31mП\033[0m\033[32mр\033[0m\033[33mИ\033[0m\033[34mк\033[0m\033[35mО\033[0m\033[36mл\033[0mА\033[37m так старался?')
player_say('Нужно успокоится')
print('''(r)Посмотреть новости
(a)Слип
(i)Сыграть в игру на консоси
(n)Поесть из "Отмороженного"''')

# парсер панорамы
url = 'https://panorama.pub/science'
req = requests.get(url)
soup = BeautifulSoup(req.text, 'html.parser')
news = soup.findAll('h3')

dialog_10 = input('~> ')
if dialog_10 == 'r':
    def watch_news():
        player_say('Щёльк')
        print('\033[36m*Сегодняшние новости!*\033[0m')
        if news:
            rnd = choice(news)
            print(f'\033[36m{rnd.text}\033[0m', end='')
            news.remove(rnd)
            input()

            if randint(0, 100) >= 70:
                print('\033[41mВы сдохли от кринжа\033[0m', end='')
                input()
                webbrowser.open_new('https://youtu.be/-fnPzAj1o_A')
                print(game_over)
                game_exit()

            conscience_say('Послушать ещё?')
            print('(r)Да\n(a)Хватит')
            if input('~> ') == 'r':
                return watch_news()
            else:
                player_say('Пойду спать')
        else:
            loading(1)
            achievement('Вакцинированный от кринжа')
            print('\033[36m*А на сегодня всё!*\033[0m', end='')
            input()


    watch_news()

elif dialog_10 == 'a':
    player_say('Спокойной ночи')
    player_say('Кому я это говорю?')

elif dialog_10 == 'i':
    conscience_say('После нескольких минут игры, ваша подписка на консось сразу же закончилась')
    conscience_say('Денег на оплату нет')
    conscience_say('Вы идёте спать')

elif dialog_10 == 'n':
    player_say('Где же мой любимый холодильничек?')
    print('\033[41mВНИМАНИЕ! ЗАМЕЧЕН МАЙОНЕЗ И ПЕЛЬПЕШКИ\033[0m')
    player_say('МММ! ПЕЛЬМЕШКИ С МАЗИКОМ!')
    conscience_say('Терпите! Они ещё готовяться ')
    loading(10)
    webbrowser.open_new('https://youtu.be/MXcMV-d_2Js')  # one hour later
    print('\033[42mГотово\033[0m', end='')
    input()
    player_say('Вкусно')
    conscience_say('Пельмешки были просрочены')
    achievement('Убитый пельменями')
    player_say('Дорогой дневник... Мне не описать ту боль, что причинили мне пельмешки')
    webbrowser.open_new('https://youtu.be/j_nV2jcTFvA')  # You died
    print(game_over)
    game_exit()

else:
    achievement('Мухожук')
    player_say('Я устал, я мухожук')
    game_exit()

clear()
print('\033[44m*Сон спит*\033[0m')
loading(2)
conscience_say('С добрым утром, человек!')
conscience_say('До сих пор ничего не помните?')
conscience_say('Я в принципе тоже не знаю')
player_say('Единственное что я чувствую, то только запах, который появляется после дождя')
player_say('Значит я сейчас нахожусь на улице')
achievement('Капитан очевидность')
player_say('Тут заметочка')
conscience_say('Прочитать?')
print('(r) Я соглашаюсь это прочитать\n(a) Мне уже всё равно, я преисполнился настолько...')
dialog_11 = input('~> ')
if dialog_11 == 'r':
    print('_-_______---____________-_-_______----______ ')
    print('|                                           |')
    print('|                                           |')
    print('|                                           |')
    print('|     Ты как                                |')
    print('|                                           |')
    print('|           смирительную рубашку снял?      |')
    print('|                                           |')
    print('|      Я одолжу твою хатку на вечность      |')
    print('|                                           |')
    print('|  Спасибо, что согласился                  |')
    print('|                                           |')
    print('|                     Лучший сосед Богдан   |')
    print('|___________________________________________|')
    input()
    player_say('Вот же мгхазь')
    player_say('БОГДАААААААН!!!')
else:
    player_say('''Я в своем познании настолько преисполнился, что я как будто бы уже
сто триллионов миллиардов лет проживаю на триллионах и
триллионах таких же планет, как эта Земля, мне этот мир абсолютно
понятен, и я здесь ищу только одного - покоя, умиротворения и
вот этой гармонии, от слияния с бесконечно вечным, от созерцания
великого фрактального подобия и от вот этого замечательного всеединства
существа, бесконечно вечного, куда ни посмотри, хоть вглубь - бесконечно
малое, хоть ввысь - бесконечное большое, понимаешь?''')

clear()
print('''█████  ██  █  ████                                          
█      ██  █  █   █                                         
█      █ █ █  █   █                                         
█████  █ █ █  █   █                                         
█      █  ██  █   █                                         
█      █  ██  █   █                                         
█████  █   █  ████                                          
                       ''')


def title(text):
    print(text)
    input()


title('А дальше уже наступил следующий день, и новые трудности для главного героя')
title('Но это уже совсем другая история...')
achievement('Легенды не умирают')

title('\033[30m\033[47mВ ГЛАВНЫХ РОЛЯХ\033[0m\033[0m')
title(f'{player_name} - Конченный идиот')
title('Богдан - Самый сексуальный мужик в мире')
title('Чертила - Горячая чикса')
title('Бомжара - Злой британец')
title('Лориэрик - Так себе шутник')
title('Деревянная Дверь - Пубертатная язва')
title('Урювкус - Какой-то мужик')
title('Гопники - Говнюки')
title('Говорящий Ботинок - Недопонятый гений')
print('Мораль всей истории заключается в...')
print('     Смотреть без гибернации и принятия ислама ==> ')
input()
webbrowser.open_new('https://youtu.be/79IghS6mlac')
print('Вы закончили игру! Можете выполнить все достижения, они находятся в файле "Прогресс.txt"')
input()
game_exit()
